package sample.Network;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiThreadServer {

    public static final int SERVER_PORT = 1488;

    static ExecutorService executorServiceForClient = Executors.newFixedThreadPool(2);

    public static void main(String[] args) throws IOException {

        try(ServerSocket serverSocket = new ServerSocket(SERVER_PORT);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

            while (!serverSocket.isClosed()) {

                if (bufferedReader.ready()) {
                    System.out.println("Server started reading the channel...");
                    String serverCommand = bufferedReader.readLine();
                    if (serverCommand.equalsIgnoreCase("exit")) {
                        System.out.println("Server finishes work.");
                        break;
                    }
                }


                Socket clientSocket = serverSocket.accept();


                executorServiceForClient.execute(new ClientHandler(clientSocket));
                System.out.println("Connection confirmed.");
            }


            executorServiceForClient.shutdown();
        }
    }
}
