package sample.Network;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main {
    public static void main(String[] args) throws InterruptedException {


        ExecutorService executorService = Executors.newFixedThreadPool(3);


        for (int i = 0; i < 10; i++) {
            executorService.execute(new RunnableClientTester());
            i++;
            Thread.sleep(2000);
        }


        executorService.shutdown();
    }
}
