package sample;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class IPEnteringWindow {

    TextField userIPData = new TextField();
    TextField userPortData = new TextField();
    Label IPLabel = new Label("IP: ");
    Label portLabel = new Label("Port: ");
    Button connectButton = new Button("Connect");
    public String display(String title) {
        Stage window = new Stage();

        // Here, we provide a dialog to configure the alertbox properties.
        //This means that when the ipentringwindow opens, we can't work with all the people.
        //Other windows until we handle ipentringwindow
        //The modification.application name u model only gives a window, and this property
        window.initModality(Modality.APPLICATION_MODAL);

        window.setTitle(title);
        window.setMaxWidth(300);
        window.setMaxHeight(200);
        window.setMinWidth(300);
        window.setMinHeight(200);

        //Set textfield length
        userIPData.setMaxWidth(105);
        userPortData.setMaxWidth(50);

        StackPane elementsLayout = new StackPane(
                userIPData,
                userPortData,
                IPLabel,
                portLabel,
                connectButton
        );

        //Place elements on stage
        StackPane.setMargin(userIPData, new Insets(0,0,100,0));
        StackPane.setMargin(userPortData, new Insets(0,0,0,0));
        StackPane.setMargin(IPLabel, new Insets(0,125,100,0));
        StackPane.setMargin(portLabel, new Insets(0,80,0,0));
        StackPane.setMargin(connectButton, new Insets(100,0,0,0));

        StringBuilder IPAndPortData = new StringBuilder();
        connectButton.setOnAction(event -> {
            IPAndPortData.append(userIPData.getText()).append(":").append(userPortData.getText());
            window.close();
        });

        Scene scene = new Scene(elementsLayout, 100, 100);
        window.setScene(scene);
        window.showAndWait();

        return IPAndPortData.toString();
    }
}
